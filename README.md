Tiny analysis of Polar heart rate data on a row interval workout.

The results are checked in under

`row_intervals_workup.html`

To rerun you need RStudio, or R at any rate. Here's a convenient docker command to make that happen, on `http://localhost:8787`, with:

  username: rstudio
  password: row

Files should be found in the `src/` directory when you do an "Open File" in RStudio.

```
#!/bin/sh

docker run \
       --interactive \
       --tty \
       --rm \
       -e PASSWORD=row \
       -p 8787:8787 \
       -v ${PWD}:/home/rstudio/src \
       rocker/tidyverse
```

